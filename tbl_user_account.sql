USE practicaDB;

TRUNCATE TABLE tbl_user_account;

--
-- +---------------------+-------------+------+-----+---------+----------------+
-- | Field               | Type        | Null | Key | Default | Extra          |
-- +---------------------+-------------+------+-----+---------+----------------+
-- | ID                  | int         | NO   | UNI | NULL    | auto_increment |
-- | nombre_usuario      | varchar(20) | NO   |     | NULL    |                |
-- | alias_usuario       | varchar(20) | NO   | PRI | NULL    |                |
-- | apellidos_usuario   | varchar(20) | NO   |     | NULL    |                |
-- | email_usuario       | varchar(20) | NO   |     | NULL    |                |
-- | contrasenia_usuario | varchar(70) | NO   |     | NULL    |                |
-- +---------------------+-------------+------+-----+---------+----------------+
--

-- Insert

INSERT INTO tbl_user_account
VALUES (NULL, 'Jean Paul', 'janpol', 'Paul', 'jeanpol@gmail.org', SHA1('password'));

INSERT INTO tbl_user_account
VALUES (NULL, 'Dennis', 'Dennis', 'Valderrama', 'dennis@gmail.com', SHA1('contraseña'));


INSERT INTO tbl_user_account
VALUES (NULL, 'Bob Dylan', 'mrrobot', 'Snowden', 'bob@tutanota.com', SHA1('contrapass'));

-- UPDATE

UPDATE tbl_user_account SET
    email_usuario = 'janpol@proton.com',
    contrasenia_usuario = SHA1('=M"v[/XnS{r2Juft')
WHERE alias_usuario = 'janpol';
