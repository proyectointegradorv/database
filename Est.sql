USE practicaDB;

TRUNCATE TABLE Est;

--

-- +------------+--------------+------+-----+---------+----------------+
-- | Field      | Type         | Null | Key | Default | Extra          |
-- +------------+--------------+------+-----+---------+----------------+
-- | cod        | int unsigned | NO   | PRI | NULL    | auto_increment |
-- | doc        | varchar(10)  | NO   | UNI | NULL    |                |
-- | tipoDoc    | varchar(2)   | NO   |     | NULL    |                |
-- | priNom     | varchar(10)  | NO   |     | NULL    |                |
-- | segNom     | varchar(10)  | YES  |     | NULL    |                |
-- | terNom     | varchar(10)  | YES  |     | NULL    |                |
-- | priApe     | varchar(10)  | NO   |     | NULL    |                |
-- | secApe     | varchar(10)  | NO   |     | NULL    |                |
-- | sexo       | varchar(10)  | NO   |     | NULL    |                |
-- | fecNac     | varchar(11)  | NO   |     | NULL    |                |
-- | lugNac     | varchar(30)  | NO   |     | NULL    |                |
-- | carrera    | varchar(30)  | NO   |     | NULL    |                |
-- | semestre   | varchar(2)   | NO   |     | NULL    |                |
-- | dirResi    | varchar(15)  | NO   |     | NULL    |                |
-- | barrioResi | varchar(15)  | NO   |     | NULL    |                |
-- | tel        | varchar(10)  | NO   |     | NULL    |                |
-- | email      | varchar(30)  | NO   |     | NULL    |                |
-- +------------+--------------+------+-----+---------+----------------+

--
-- INSERT

INSERT INTO Est VALUES(NULL, '0111000111', 'CC', 'Juan', 'Pablo', 'Ángel', 'Parra', 'Arévalo', 'M', '11-11-11', 'Bogotá', 'Sistemas', 'V', 'cra111', 'Barrio', '123', 'Email');

INSERT INTO Est VALUES(NULL, '1234567890', 'TI', 'Dylan', 'Steven', NULL, 'Romero', 'Romero', 'F', '10-10-10', 'Bogotá', 'Sistemas', 'V', 'cra222', 'Barrio', '3204160629', 'dylanr@sanmateo.edu.co');

INSERT INTO Est VALUES(NULL, '0123456789', 'CE', 'Daniel', 'Santiago', NULL, 'Valderrama', 'Álvarez', 'M', '9-9-9', 'Bogotá', 'Sistemas', 'V', 'cra333', 'Barrio', '3243621000', 'dennis@sanmateo.edu.co');

INSERT INTO Est VALUES(NULL, '24681012', 'TI', 'Brayan', 'Nicolás', NULL, 'Rojas', 'Tinoco', 'M', '7-10-2014', 'The Table', 'Sistemas', 'II', 'cll135', 'The', '2345670', 'bnrojasr@sanmateo.edu.co');

INSERT INTO Est VALUES(NULL, '9876543210', 'CC', 'Juan', 'Carlos', NULL, 'Sánchez', 'Tovar', 'M', '4-12-2000', 'Bogotá', 'Sistemas', 'II', 'cll136', 'Toberín', '1239870', 'juancesete@gmail.com');

INSERT INTO Est VALUES(NULL, '1000100010', 'CC', 'Juana', NULL, NULL, 'Parra', 'Torres', 'F', '21-12-2012', 'Cartagena', 'Telecomunicaciones', 'VI', 'Turquía', 'Estambul', '12345345', 'juana@mail.com');

INSERT INTO Est VALUES(NULL, '1007878231', 'CC', 'Dayanne', 'Stephany', NULL, 'Romero', 'Romero', 'F', '9-9-9', 'Bogotá', 'Diseño gráfico', 'X', 'Japón', 'Tokyo', '3204160629', 'dallanne@sanmateo.edu.co');

INSERT INTO Est VALUES(NULL, '0235981232', 'CE', 'Daniela', NULL, NULL, 'Valderrama', 'Álvarez', 'F', '4-1-44', 'Estocolmo', 'Gastronomía', 'I', 'Suecia', 'suecia1', '3243621000', 'dennisia@sanmateo.edu.co');

INSERT INTO Est VALUES(NULL, '245780184', 'TI', 'Brigitte', 'Nicolle', NULL, 'Blanco', 'Tinoco', 'F', '4-5-2003', 'Buenos aires', 'Sistemas', 'II', 'cll135', 'Argentina', '2345671890', 'brigitte@sanmateo.edu.co');

INSERT INTO Est VALUES(NULL, '987654325', 'CC', 'Johana', 'Carlota', NULL, 'Sánchez', 'Tovar', 'F', '12-4-2001', 'moscú', 'Sistemas', 'VI', 'cra 124', 'Rusia', '123982342', 'geici@gmail.com');

-- UPDATE
UPDATE Est SET
    email = 'test@protonmail.com'
WHERE doc = '0111000111';

UPDATE Est SET
    tel = '234546798',
    email = 'tu@tutanota.org'
WHERE doc = '245780184';


UPDATE Est SET
    segNom = 'Josefina'
WHERE doc = '0235981232';

-- DELETE
DELETE FROM Est WHERE doc = '1000100010';
