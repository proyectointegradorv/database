-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-05-2021 a las 20:10:08
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

DROP DATABASE IF EXISTS practicaDB;

CREATE DATABASE practicaDB;

USE practicaDB;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practicadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `est`
--

CREATE TABLE `est` (
  `cod` int(10) UNSIGNED NOT NULL,
  `doc` varchar(10) NOT NULL,
  `tipoDoc` varchar(2) NOT NULL,
  `priNom` varchar(10) NOT NULL,
  `segNom` varchar(10) DEFAULT NULL,
  `terNom` varchar(10) DEFAULT NULL,
  `priApe` varchar(10) NOT NULL,
  `secApe` varchar(10) NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `fecNac` varchar(11) NOT NULL,
  `lugNac` varchar(30) NOT NULL,
  `carrera` varchar(30) NOT NULL,
  `semestre` varchar(2) NOT NULL,
  `dirResi` varchar(15) NOT NULL,
  `barrioResi` varchar(15) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `est`
--
DELIMITER $$
CREATE TRIGGER `DeleteStudent` AFTER DELETE ON `est` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'DELETE', 'Estudiante')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `InsertStudent` AFTER INSERT ON `est` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'INSERT', 'Estudiante')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `UpdateStudent` AFTER UPDATE ON `est` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'UPDATE', 'Estudiante')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `cod` int(10) UNSIGNED NOT NULL,
  `uname` varchar(30) NOT NULL,
  `dateLog` varchar(30) NOT NULL,
  `timeLog` varchar(30) NOT NULL,
  `typeLog` varchar(10) NOT NULL,
  `query` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_empresa`
--

CREATE TABLE `tbl_empresa` (
  `ID` int(15) NOT NULL,
  `NIT` int(15) NOT NULL,
  `nombre_empresa` varchar(30) NOT NULL,
  `ciudad_sede` varchar(22) DEFAULT NULL,
  `departamento_sede` varchar(22) DEFAULT NULL,
  `pais_sede` varchar(22) DEFAULT NULL,
  `tipo_empresa` varchar(10) NOT NULL,
  `direccion_sede` varchar(50) NOT NULL,
  `tipo_contacto` varchar(15) DEFAULT NULL,
  `numero_contacto` int(15) NOT NULL,
  `website_contacto` varchar(200) DEFAULT NULL,
  `estado_empresa` varchar(12) NOT NULL,
  `origen_empresa` varchar(88) DEFAULT NULL,
  `numero_empleados` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Disparadores `tbl_empresa`
--
DELIMITER $$
CREATE TRIGGER `DeleteCompany` AFTER DELETE ON `tbl_empresa` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'DELETE', 'Company Deleted')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `InsertCompany` AFTER INSERT ON `tbl_empresa` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'INSERT', 'Company Created')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `UpdateCompany` AFTER UPDATE ON `tbl_empresa` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'UPDATE', 'Company Updated')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_user_account`
--

CREATE TABLE `tbl_user_account` (
  `ID` int(15) NOT NULL,
  `nombre_usuario` varchar(20) NOT NULL,
  `alias_usuario` varchar(20) NOT NULL,
  `apellidos_usuario` varchar(20) NOT NULL,
  `email_usuario` varchar(50) NOT NULL,
  `contrasenia_usuario` varchar(180) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `tbl_user_account`
--
DELIMITER $$
CREATE TRIGGER `DeleteUserAccount` AFTER DELETE ON `tbl_user_account` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'DELETE', 'User Account Deleted')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `InsertUserAccount` AFTER INSERT ON `tbl_user_account` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'INSERT', 'User Account Created')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `UpdateUserAccount` AFTER UPDATE ON `tbl_user_account` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'UPDATE', 'User Account Updated')
$$
DELIMITER ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `cod` int(10) UNSIGNED NOT NULL,
  `uname` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `users`
--
DELIMITER $$
CREATE TRIGGER `DeleteUser` AFTER DELETE ON `users` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'DELETE', 'User')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `InsertUser` AFTER INSERT ON `users` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'INSERT', 'User')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `UpdateUser` AFTER UPDATE ON `users` FOR EACH ROW INSERT INTO Logs (cod, uname, dateLog, timeLog, typeLog, query)
        VALUES (NULL, CURRENT_USER(), CURRENT_DATE(), CURRENT_TIME(), 'UPDATE', 'User')
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `est`
--
ALTER TABLE `est`
  ADD PRIMARY KEY (`cod`),
  ADD UNIQUE KEY `doc` (`doc`);

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`cod`);

--
-- Indices de la tabla `tbl_empresa`
--
ALTER TABLE `tbl_empresa`
  ADD PRIMARY KEY (`NIT`),
  ADD UNIQUE KEY `UNIQUE` (`nombre_empresa`,`tipo_empresa`,`direccion_sede`,`numero_contacto`,`estado_empresa`),
  ADD UNIQUE KEY `UNIQUE_KEY` (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indices de la tabla `tbl_user_account`
--
ALTER TABLE `tbl_user_account`
  ADD PRIMARY KEY (`alias_usuario`),
  ADD UNIQUE KEY `tbl_user_account_ID_uindex` (`ID`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`cod`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `est`
--
ALTER TABLE `est`
  MODIFY `cod` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `logs`
--
ALTER TABLE `logs`
  MODIFY `cod` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_empresa`
--
ALTER TABLE `tbl_empresa`
  MODIFY `ID` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_user_account`
--
ALTER TABLE `tbl_user_account`
  MODIFY `ID` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `cod` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
