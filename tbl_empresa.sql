USE practicaDB;

TRUNCATE TABLE tbl_empresa;

--
-- +-------------------+--------------+------+-----+---------+----------------+
-- | Field             | Type         | Null | Key | Default | Extra          |
-- +-------------------+--------------+------+-----+---------+----------------+
-- | ID                | int          | NO   | UNI | NULL    | auto_increment |
-- | NIT               | int          | NO   | PRI | NULL    |                |
-- | nombre_empresa    | varchar(30)  | NO   | MUL | NULL    |                |
-- | ciudad_sede       | varchar(22)  | YES  |     | NULL    |                |
-- | departamento_sede | varchar(22)  | YES  |     | NULL    |                |
-- | pais_sede         | varchar(22)  | YES  |     | NULL    |                |
-- | tipo_empresa      | varchar(10)  | NO   |     | NULL    |                |
-- | direccion_sede    | varchar(50)  | NO   |     | NULL    |                |
-- | tipo_contacto     | varchar(15)  | YES  |     | NULL    |                |
-- | numero_contacto   | int          | NO   |     | NULL    |                |
-- | website_contacto  | varchar(200) | YES  |     | NULL    |                |
-- | estado_empresa    | varchar(12)  | NO   |     | NULL    |                |
-- | origen_empresa    | varchar(88)  | YES  |     | NULL    |                |
-- | numero_empleados  | varchar(40)  | YES  |     | NULL    |                |
-- +-------------------+--------------+------+-----+---------+----------------+
--

-- INSERT

INSERT INTO tbl_empresa
VALUES (NULL, 105865, 'Pijamas Maestras', 'Bogotá', 'Cundinamarca', 'Colombia', 'Ropa', 'Cra 1236540', 'Correo', 138, 'localohst', 'Sólido', 'Fundador', '340');

INSERT INTO tbl_empresa
VALUES (NULL, 890924, 'Servicio al cliente', NULL, NULL, NULL, 'Servicio', 'Cra 8923 d 1238', 'Correo', 198745, '127.0.0.1', 'Sólido', 'Fundador', '420');

INSERT INTO tbl_empresa
VALUES (NULL, 105234909, 'Audio solutions', 'Madrid', NULL, 'Esapaña', 'Multimedia', 'Cra 45 #7sdf', 'Correo', 88927340, 'localohst', 'Sólido', 'Fundador', '567');

INSERT INTO tbl_empresa
VALUES (NULL, 78378378, 'Software House', 'Nueva Dheli', NULL, 'India', 'Software', 'Cll west carl', 'Correo', 7898340, '127.0.0.1', 'Sólido', 'Fundador', '600');

INSERT INTO tbl_empresa
VALUES (NULL, 2349871, 'Very Hard Hardware', 'Taipei', NULL, 'Taiwán', 'Hardware', 'Avenue st.', 'Correo', 234879, 'localohst', 'Sólido', 'Fundador', '090');

-- UPDATE

UPDATE tbl_empresa SET
    direccion_sede = '298 # st',
    numero_contacto = 19367083,
    numero_empleados = '700'
WHERE NIT = 105865;

-- DELETE

DELETE FROM tbl_empresa WHERE NIT = 890924;
